nmly
====

## About

`nmly` is a batch rename utility [originally written](https://github.com/Usbac/nmly) by [Alejandro "Usbac"](https://github.com/Usbac/). This fork has some restrictions removed and therefore may be seen as easier to run. However, I strongly encourage you to use the original program and naturally all the credits go to the original author unless stated otherwise.


## Changes

  - `nmly` doesn't ask you for a confirmation if the files being renamed are located in the current directory (no other directory is provided via `-d` / `--directory` options);
  - if asked for a confirmation, `nmly` assumes `Yes` as the default answer and therefore simply hitting `<Enter>` is enough.

